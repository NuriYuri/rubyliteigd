# Ruby-LiteIGD
A little ruby implementation of the IGD protocol (only functions that helps to open ports).
## How to use LiteIGD ?
First of all you have to require LiteIGD by using `require 'LiteIGD'`.

In order to use the LiteIGD function you need to locate the IGD :
```ruby
unless(LiteIGD.search_WANConnection)
  # IGD not found, no LiteIGD operation available
end
```

If `LiteIGD.search_WANConnection` returns true, you can use the following function : 
- `LiteIGD.get_external_ip_address`
- `LiteIGD.delete_port(external_port, protocol, remote_host)`
- `LiteIGD.add_port(external_port, protocol, internal_port, internal_ip, description, duration, enabled, remote_host)`
- `LiteIGD.each_port { |port|  }` port is an `LiteIGD::PortMapping` object
- `LiteIGD.get_port(external_port, protocol, remote_host)` return a `LiteIGD::PortMapping` object if the operation was a success.

## What is a LiteIGD::PortMapping ?
Its an object that has as attribute the arguments of `LiteIGD.add_port` :
- `LiteIGD::PortMapping#external_port`
- `LiteIGD::PortMapping#protocol`
- `LiteIGD::PortMapping#internal_port`
- `LiteIGD::PortMapping#internal_ip`
- `LiteIGD::PortMapping#description`
- `LiteIGD::PortMapping#duration`
- `LiteIGD::PortMapping#enabled`
- `LiteIGD::PortMapping#remote_host`

## How to get computer IP ?
In order to add port you need to retreive the internal_ip, LiteIGD has a function for that :
```ruby
LiteIGD.get_internal_ip_address
```
You can use this function anytime (thus if LiteIGD.search_WANConnection returns false, you can do stuff with the internal_ip instead of the external_ip).

## Things to know
- In most of IGD you cannot open an external port below 1024
- Some IGD doesn't allow opening an external port that is different from the internal port
- Some IGD disallow the operation of deleting a port that was not openned with your internal ip.